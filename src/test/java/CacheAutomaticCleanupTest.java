import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jas on 2014-11-30.
 * <p/>
 * Issues:
 * 1. some copy-paste in methods
 * 2. Test strongly depend on performance / time execution. Should consider different approach to test. (Mockito, verify method calls)
 * 3. More tc's needed
 */
public class CacheAutomaticCleanupTest extends TestCase {

    SimpleCache<Integer, CachedObject> integerObjectCache;
    long timeToLive = 60 * 10;
    long timeMargin = 200;

    @Before
    public void setUp() {
        integerObjectCache = CacheManager.getInstance().createCache(timeToLive, timeToLive / 4);
    }

    @Test
    public void testPutGetSimple() {
        CachedObject o = new CachedObject();
        integerObjectCache.put(1, o);
        Optional<CachedObject> oFromCache = integerObjectCache.get(1);
        if (oFromCache.isPresent()) {
            assertEquals("Object obtained from cache is the same object", o, oFromCache.get());
        } else {
            fail("Object obtained from cache is a different object!");
        }
    }

    @Test
    public void testPutGetSimpleExpired() throws InterruptedException {
        CachedObject o = new CachedObject();
        integerObjectCache.put(1, o);

        Thread.sleep(timeToLive + timeMargin);

        Optional<CachedObject> oFromCache = integerObjectCache.get(1);
        if (oFromCache.isPresent()) {
            fail("Object in cache should be expired!");
        }
    }

    @Test
    public void testPutCheckExpiredCleanup() throws InterruptedException, NoSuchFieldException, IllegalAccessException {
        CachedObject o = new CachedObject();
        integerObjectCache.put(1, o);

        Thread.sleep(timeToLive + timeMargin);

        Field privateCacheField = CacheAutomaticCleanup.class.getDeclaredField("cache");
        privateCacheField.setAccessible(true);
        Map<Integer, Object> cacheInternal = (ConcurrentHashMap<Integer, Object>) privateCacheField.get(integerObjectCache);

        if (!cacheInternal.isEmpty()) {
            fail("Object in cache should expire and be removed!");
        }
    }

    @Test
    public void testSetExpired() {
        CachedObject o = new CachedObject();
        integerObjectCache.put(1, o);
        o.setExpired();
        Optional<CachedObject> oFromCache = integerObjectCache.get(1);
        if (oFromCache.isPresent()) {
            fail("Object in cache should be expired on removed on geet!");
        }
    }

    class CachedObject implements Expirable {
        private volatile boolean isExpired = false;

        @Override
        public boolean isExpired() {
            return isExpired;
        }

        void setExpired() {
            isExpired = true;
        }

    }
}

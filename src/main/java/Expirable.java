/**
 * Created by jas on 2014-11-30.
 */
interface Expirable {

    boolean isExpired();
}

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jas on 2014-11-30.
 * <p/>
 * Assumptions:
 * 1. Objects in cache expire when time measured from putting object into cache exceeds TTL constant
 * 2. Objects can expire based on their internal state (Expirable interface)
 * <p/>
 * Issues:
 * 1. cleanup method is accessible, its not an error but still something to keep in mind
 */
class CacheAutomaticCleanup<K, V extends Expirable> implements SimpleCache<K, V> {

    private Map<K, ValueWrapper> cache = new ConcurrentHashMap<>();
    private volatile boolean used = true;
    private final long timeToLive;

    CacheAutomaticCleanup(long timeToLive) {
        this.timeToLive = timeToLive;
    }

    @Override
    public void put(K key, V value) {
        if (key == null || value == null) {
            throw new IllegalArgumentException();
        }
        cache.put(key, new ValueWrapper(value, System.currentTimeMillis()));
    }

    @Override
    public Optional<V> get(K key) {
        ValueWrapper wrappedValue = cache.get(key);
        if (wrappedValue == null || isExpired(wrappedValue)) {
            cache.remove(key);
            return Optional.empty();
        }
        return Optional.of(cache.get(key).getValue());
    }

    @Override
    public void setUnused() {
        this.used = false;
    }

    @Override
    public boolean isUsed() {
        return this.used;
    }

    void cleanup() {
        for (Map.Entry<K, ValueWrapper> entry : cache.entrySet()) {
            if (isExpired(entry.getValue())) {
                cache.remove(entry.getKey());
            }
        }
    }

    private boolean isExpired(final ValueWrapper valueWrapper) {
        if ((System.currentTimeMillis() - valueWrapper.getPutTime()) > this.timeToLive ||
                valueWrapper.getValue().isExpired()) {
            return true;
        }
        return false;
    }

    private class ValueWrapper {
        private final V value;
        private final long putTime;

        ValueWrapper(V value, long putTime) {
            this.value = value;
            this.putTime = putTime;
        }

        V getValue() {
            return value;
        }

        long getPutTime() {
            return putTime;
        }
    }
}

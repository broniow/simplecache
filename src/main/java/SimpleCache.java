import java.util.Optional;

/**
 * Created by jas on 2014-11-30.
 *
 *
 *
 */
interface SimpleCache <K, V extends Expirable> {

    void put(K key, V value) throws IllegalArgumentException;

    Optional<V> get(K key);

    void setUnused();

    boolean isUsed();
}

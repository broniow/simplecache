/**
 * Created by jas on 2014-11-30.
 */
public class CacheAutomaticCleanupThread extends Thread {

    private CacheAutomaticCleanup cache;
    private long cleanupInterval;

    CacheAutomaticCleanupThread(CacheAutomaticCleanup cache, long cleanupInterval) {
        this.cache = cache;
        this.cleanupInterval = cleanupInterval;
    }

    @Override
    public void run() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                cache.cleanup();
                Thread.sleep(cleanupInterval);
            }
        } catch (InterruptedException e) {
            // cleanup job interruped!
            // Probably should block put method or something, right now cache can grow and grow
        }
    }
}

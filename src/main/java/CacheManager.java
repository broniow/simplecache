import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jas on 2014-11-30.
 *
 * Issues:
 * 1. No fall back mechanisms, a lot of todos
 *
 */
class CacheManager {

    private static CacheManager instance = new CacheManager();
    private static Map<SimpleCache, Thread> createdCaches = new ConcurrentHashMap<>();

    public static CacheManager getInstance(){
        return instance;
    }

    private CacheManager() {
        new CachesMonitorThread().start();
    }

    <K, V extends Expirable> SimpleCache<K, V> createCache() {
        long timeToLive = 60 * 1000; // 1 min
        long cleanupInterval = timeToLive / 4;
        return CacheManager.getInstance().createCache(timeToLive, cleanupInterval);
    }

    <K, V extends Expirable> SimpleCache<K, V> createCache(long timeToLive, long cleanupInterval) {
        CacheAutomaticCleanup<K, V> cache = new CacheAutomaticCleanup<>(timeToLive);
        CacheAutomaticCleanupThread cleanupThread = new CacheAutomaticCleanupThread(cache, cleanupInterval);
        cleanupThread.start();
        createdCaches.put(cache, cleanupThread);
        return cache;
    }

    // simple thread for stopping not used caches threads
    private class CachesMonitorThread extends Thread {

        public CachesMonitorThread() {
            setDaemon(true) ;
        }

        public void run() {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    for (Map.Entry<SimpleCache, Thread> entry : createdCaches.entrySet()) {
                        if (!entry.getKey().isUsed()) {
                            entry.getValue().interrupt();
                            createdCaches.remove(entry.getKey());
                        }
                    }
                    Thread.sleep(1000 * 60 * 5); //5 mins
                }
            } catch (InterruptedException e) {
                // todo
            }
        }
    }

}
